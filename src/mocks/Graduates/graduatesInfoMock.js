const folder = "Graduates/";
const fileExtension = ".svg";

const graduatesInfoMock = [
  {
    name: "Дарья",
    surname: "Жарковская",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Zharkovskaya_Daria${fileExtension}`
  },
  {
    name: "Дарья",
    surname: "Кобозева",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Kobozeva_Darya${fileExtension}`
  },
  {
    name: "Виктор",
    surname: "Шабельник",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Shabelnik_Viktor${fileExtension}`
  },
  {
    name: "Сергей",
    surname: "Шелудяков",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Sergey_Sheludyakov${fileExtension}`
  },
  {
    name: "Сергей",
    surname: "Тимошенков",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Sergey_Timoshenkov${fileExtension}`
  },
  {
    name: "Виталий",
    surname: "Бабурченков",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Vitaly_Baburchenkova${fileExtension}`
  },
  {
    name: "Юля",
    surname: "Петрова",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Julia_Petrova${fileExtension}`
  },
  {
    name: "Юлия",
    surname: "Попова",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Julia_Popova${fileExtension}`
  },
  {
    name: "Юлия",
    surname: "Пивнова",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Julia_Piunova${fileExtension}`
  },
  {
    name: "Евгения",
    surname: "Грязнова",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Evgeny_Gryaznov${fileExtension}`
  },
  {
    name: "Анна",
    surname: "Соваренко",
    year: "",
    specialty: "",
    description: "",
    imagePath: `${folder}Anna_Soforenko${fileExtension}`
  }
];

export default graduatesInfoMock;