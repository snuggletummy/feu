import InstagramIcon from '@material-ui/icons/Instagram';
import FacebookIcon from '@material-ui/icons/Facebook';
import TwitterIcon from '@material-ui/icons/Twitter';

import React from "react";

const socialMediasMock = [
  { href: "https://instagram.com/", icon: <InstagramIcon /> },
  { href: "https://facebook.com/", icon: <FacebookIcon /> },
  { href: "https://twitter.com/", icon: <TwitterIcon /> }
]

export default socialMediasMock;