const departmentOfAnalyticalAndDigitalTechnologies = [
  {
    name: "Дмитрий",
    surname: "Букачев",
    patronymic: "Сергеевич",
    description: "заведующий кафедрой, кандидат физико-математических наук",
    courses: [
      "информационные технологии в экономике",
      "информационные системы в экономике",
      "безопасность электронного документооборота",
      "основы информационной безопасности",
      "основы искусственного интеллекта",
      "лабораторный практикум «1С: Предприятие»",
    ],
    interests: [
      "проектирование информационных систем",
      "высокоуровневые методы программирования",
      "базы данных",
      "информационная безопасность",
      "облачные технологии в экономике",
      "управление цифровыми активами",
      "Data Science",
      "Machine Learning",
    ],
    image: "bukachev",
  },
  {
    name: "Михаил",
    surname: "Банару",
    patronymic: "Борисович",
    description: "кандидат физико-математических наук, доцент",
    courses: [
      "теория вероятностей и математическая статистика",
      "линейная алгебра",
      "математика",
      "высшая математика",
    ],
    interests: [
      "дифференциальная геометрия",
      "методика преподавания математики в вузе и старших классах",
    ],
    image: "banary",
  },
  {
    name: "Дмитрий",
    surname: "Бояринов",
    patronymic: "Анатольевич",
    description: "кандидат педагогических наук, доцент",
    courses: [
      "математика",
      "теория вероятностей и математическая статистика",
      "теория игр",
      "линейная алгебра",
      "дискретные и непрерывные модели",
      "математические модели в экономических исследованиях",
      "математические методы в психологии",
      "информационно-коммуникационные технологии",
      "информационные таможенные технологии",
      "основы информатики",
      "информационные технологии в профессиональной деятельности",
      "интернет-технологии",
      "информационные ресурсы и технологии в маркетинге",
      "электронное правительство",
      "информационные технологи управления проектами",
      "электронный документооборот",
    ],
    interests: [
      "новые информационные технологии в педагогике",
      "дискретная математика",
    ],
    image: "boyarinov",
  },
  {
    name: "Наталья",
    surname: "Перельман",
    patronymic: "Романовна",
    description: "кандидат физико-математических наук, доцент",
    courses: [
      "математика",
      "математический анализ",
      "теория вероятностей и математическая статистика",
      "линейная алгебра",
      "методы принятия управленческих решений",
      "основы финансовой математики",
      "основы математической экономики",
      "математические основы финансовый расчетов",
      "методы оптимальных решений",
      "математическое моделирование социально-экономических процессов",
    ],
    interests: [
      "краевые задачи комплексного анализа",
      "преподавание математики в высшей школе",
    ],
    image: "perelman",
  },
  {
    name: "Наталья",
    surname: "Савченкова",
    patronymic: "Николаевна",
    description: "кандидат педагогических наук, доцент",
    courses: [
      "информатика",
      "математика и информатика",
      "информационно-коммуникационные технологии",
      "математика",
      "информационные технологии в экономике",
    ],
    interests: [
      "педагогические измерения",
      "учебная диагностика",
      "тестологическая культура",
      "образовательный менеджмент",
      "дистанционные образовательные технологии",
    ],
    image: "savchenkova",
  },
  {
    name: "Станислав",
    surname: "Ксенофонтов",
    patronymic: "Андреевич",
    description: "старший преподаватель",
    courses: [
      "эконометрика",
      "теория вероятностей и математическая статистика",
      "математическое моделирование социальных процессов",
      "методы прикладной статистики для социологов",
    ],
    interests: [
      "математическое моделирование социально-экономических процессов",
      "статистический анализ фондов",
      "Big Data",
    ],
    image: "ksenofontov",
  },
  {
    name: "Анатолий",
    surname: "Ефремов",
    patronymic: "Константинович",
    description: "заведующий лабораториями",
    courses: [],
    interests: [],
    image: "efremov",
  },
  {
    name: "Николай",
    surname: "Беляев",
    patronymic: "Александрович",
    description: "лаборант",
    courses: [],
    interests: [],
    image: "belyaev",
  },
  {
    name: "Виктория",
    surname: "Себенкова",
    patronymic: "Олеговна",
    description: "лаборант",
    courses: [],
    interests: [],
    image: "sebenkova",
  },
  {
    name: "Римма",
    surname: "Тиканова",
    patronymic: "Исааковна",
    description: "лаборант",
    courses: [],
    interests: [],
    image: "tikanova",
  },
].map((info) => ({
  ...info,
  image: `Teachers/DepartmentOfAnalyticalAndDigitalTechnologies/${info.image}.jpg`,
}));

export default departmentOfAnalyticalAndDigitalTechnologies;
