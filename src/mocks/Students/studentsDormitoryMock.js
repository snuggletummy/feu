const studentsDormitoryMock = {
  links: [
    {
      href: "http://www.smolgu.ru/files/doc/student/pologenie_ob.pdf",
      text: "Положение о студенческом общежитии"
    },
    {
      href: "http://www.smolgu.ru/files/doc/student/pologenie_studsovet.pdf",
      text: "Положение о Студенческом совете федерального государственного бюджетного образовательного учреждения высшего профессионального образования «Смоленский государственный университет»"
    },
    {
      href: "http://www.smolgu.ru/files/doc/student/prikaz.pdf",
      text: "Приказ о стоимости проживания в студенческих общежитиях"
    },
    {
      href: "http://www.smolgu.ru/files/doc/student/dogovor_naima.pdf",
      text: "Образец договора найма жилого помещения в общежитии"
    },
    {
      href: "http://www.smolgu.ru/files/doc/student/kvit_ob.pdf",
      text: "Образец квитанции оплаты за общежитие"
    },
    {
      href: "http://www.smolgu.ru/files/doc/student/svedenia_ob.pdf",
      text: "Сведения о жилых помещениях в студенческих общежитиях"
    }
  ]
}

export default studentsDormitoryMock;