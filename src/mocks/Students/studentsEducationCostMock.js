const studentsEducationCostMock = {
    links: [
      {
        href: "http://www.smolgu.ru/files/doc/normativ/stoimost_1k.pdf",
        text: "Стоимость обучения в СмолГУ по договорам на оказание платных образовательных услуг в 2019-2020 учебном году (1 курс)"
      },
      {
        href: "http://www.smolgu.ru/files/doc/normativ/stoimost_2-5k.pdf",
        text: "Стоимость обучения в СмолГУ по договорам на оказание платных образовательных услуг в 2019-2020 учебном году (2 - 5 курсы)"
      }
    ]
  }
  
  export default studentsEducationCostMock;