const newsMock = [
  {
    title: "'Наука' ищет лидеров нового поколения в СмолГУ",
    date: "1 мая 2021",
    text: "Учёных Смоленского госуниверситета приглашают принять участие в конкурсе «Лидеры России». Одним из специальных треков станет трек «Наука», реализуемый совместно АНО «Россия – страна возможностей» и Координационным советом по делам молодёжи в научной и образовательной сферах Совета при Президенте РФ по науке и образованию при поддержке Минобрнауки России.",
    tags: ["Наука", "Студенту", "Жизнь факультета"],
  },
  {
    title: "'Наука' ищет лидеров нового поколения в СмолГУ",
    date: "1 мая 2021",
    text: "Учёных Смоленского госуниверситета приглашают принять участие в конкурсе «Лидеры России». Одним из специальных треков станет трек «Наука», реализуемый совместно АНО «Россия – страна возможностей» и Координационным советом по делам молодёжи в научной и образовательной сферах Совета при Президенте РФ по науке и образованию при поддержке Минобрнауки России.",
    tags: ["Наука", "Студенту", "Жизнь факультета"],
  },
  {
    title: "'Наука' ищет лидеров нового поколения в СмолГУ",
    date: "1 мая 2021",
    text: "Учёных Смоленского госуниверситета приглашают принять участие в конкурсе «Лидеры России». Одним из специальных треков станет трек «Наука», реализуемый совместно АНО «Россия – страна возможностей» и Координационным советом по делам молодёжи в научной и образовательной сферах Совета при Президенте РФ по науке и образованию при поддержке Минобрнауки России.",
    tags: ["Наука", "Студенту", "Жизнь факультета"],
  },
  {
    title: "'Наука' ищет лидеров нового поколения в СмолГУ",
    date: "1 мая 2021",
    text: "Учёных Смоленского госуниверситета приглашают принять участие в конкурсе «Лидеры России». Одним из специальных треков станет трек «Наука», реализуемый совместно АНО «Россия – страна возможностей» и Координационным советом по делам молодёжи в научной и образовательной сферах Совета при Президенте РФ по науке и образованию при поддержке Минобрнауки России.",
    tags: ["Наука", "Студенту", "Жизнь факультета"],
  },
];

export default newsMock;
