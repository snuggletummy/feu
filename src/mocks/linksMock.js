const linksMock = {
  firstColumn: [
    {
      key: 1,
      title: "Сроки проведения приёма бакалавриат",
      link: "http://abiturient.smolgu.ru/print/sroki-provedeniya-priema-po-programmam-bakalavriata-ochnoy-formy-obucheniya"
    },
    {
      key: 2,
      title: "Сроки проведения приёма магистратура",
      link: "http://abiturient.smolgu.ru/print/sroki-provedeniya-priema-po-programmam-magistratury"
    },
    {
      key: 3,
      title: "Сроки проведения приёма специалитет",
      link: "http://abiturient.smolgu.ru/print/sroki-provedeniya-priema-po-programmam-bakalavriata-ochnoy-formy-obucheniya"
    },
    {
      key: 4,
      title: "Документы, необходимые для поступления",
      link: "http://abiturient.smolgu.ru/print/dokumenty-neobkhodimye-dlya-postupleniya"
    },
    {
      key: 5,
      title: "Информация о медицинском осмотре",
      link: "http://abiturient.smolgu.ru/print/informatsiya-o-meditsinskom-osmotre"
    },
    {
      key: 6,
      title: "Информация о наличии общежитий",
      link: "http://abiturient.smolgu.ru/print/informatsiya-o-nalichii-obshchezhitiy"
    },
    {
      key: 7,
      title: "Подача документов в электронной форме",
      link: "http://abiturient.smolgu.ru/print/podacha-dokumentov-elektronnoy-forme"
    },
  ],
  secondColumn: [
    {
      key: 8,
      title: "Порядок учета индивидуальных достижений (бакалавриат, специалитет)",
      link: "http://abiturient.smolgu.ru/print/uchet-individualnykh-dostizheniy"
    },
    {
      key: 9,
      title: "Порядок учета индивидуальных достижений (магистратура)",
      link: "http://abiturient.smolgu.ru/print/poryadok-ucheta-individualnykh-dostizheniy-magistratura"
    },
    {
      key: 10,
      title: "Информация об особенностях проведения вступительных испытаний для лиц с ограниченными возможностями здоровья и инвалидов",
      link: "http://abiturient.smolgu.ru/print/vstupitelnye-ispytaniya-dlya-invalidov"
    },
    {
      key: 11,
      title: "Информация об особых правах и преимуществах, предоставляемых поступающим",
      link: "http://abiturient.smolgu.ru/print/informatsiya-ob-osobykh-pravakh"
    },
    {
      key: 12,
      title: "Информация о предоставлении особых прав и преимуществ, обусловленных олимпиадами школьников",
      link: "http://abiturient.smolgu.ru/print/informatsiya-o-predostavlenii-osobykh-prav-i-preimushchestv-obuslovlennykh-olimpiadami-shkolnikov"
    }

  ]
}

export default linksMock;