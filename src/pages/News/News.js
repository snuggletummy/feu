import React from "react";
import PageTitle from "../../components/shared/Titles/PageTitle";
import Divider from "../../components/shared/Divider/Divider";
import { Grid, CardMedia, Box, Chip } from "@material-ui/core";
import logo from "../../assets/News/smolgu.jpg";
import newsMock from "../../mocks/News/newsMock";

const News = () => {
  return (
    <React.Fragment>
      <PageTitle text="Новости" textAlign="center" />
      <Divider />
      <Grid container>
        {newsMock.map((news) => {
          return (
            <Grid container item xs={12}>
              <Grid item xs={4}>
                <CardMedia
                  component="img"
                  style={{
                    height: 200,
                  }}
                  alt="Новость"
                  image={logo}
                  title="Новость"
                />
              </Grid>
              <Grid item xs={8}>
                <Box mb={8} paddingTop={5}>
                  <Box mb={2} fontWeight="bold" display="flex">
                    <Box flexGrow={1}>{news.title}</Box>
                    <Box>{news.date}</Box>
                  </Box>
                  <Box mb={2}>{news.text}</Box>
                  <Box display="flex">
                    {news.tags.map((tag) => {
                      return (
                        <Box mr={1}>
                          <Chip color="primary" label={tag} />
                        </Box>
                      );
                    })}
                  </Box>
                </Box>
              </Grid>
            </Grid>
          );
        })}
      </Grid>
    </React.Fragment>
  );
};

export default News;
