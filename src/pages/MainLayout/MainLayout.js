import React from "react";
import Box from "@material-ui/core/Box";
import Header from "../../components/Header";
import Footer from "../../components/Footer";
import { Container } from "@material-ui/core";

export default function MainLayout(props) {
  const { children } = props;

  return (
    <Container>
      <Box>
        <Header />
      </Box>
      <Box mb={4}>{children}</Box>
      <Box>
        <Footer />
      </Box>
    </Container>
  );
}
