import React from "react";
import AboutHistory from "../../components/AboutHistory";
import AboutCommonInfo from "../../components/AboutCommonInfo";
import AboutInfographics from "../../components/AboutInfographics";
import AboutAchievements from "../../components/AboutAchievements";

const About = (props) => {
  return (
    <React.Fragment>
      <AboutHistory />
      <AboutInfographics />
      <AboutCommonInfo text="Основная задача факультета экономики и управления обеспечить подготовку квалифицированных специалистов, способных осуществлять эффективную организацию систем управления, совершенствование управления в рамках отдельных организаций, учреждений, предприятий. За 20 летнию историю,факультет закрепил за собой лидерство по показателям научно-исследовательской работы." />
      <AboutAchievements />
      <AboutCommonInfo text="Выпускники факультета работают в разных сферах: банках, учреждениях, организациях разных форм собственности, государственных и муниципальных органах власти." />
    </React.Fragment>
  );
};

export default About;
