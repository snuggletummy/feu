import React from "react";
import PageTitle from "../../components/shared/Titles/PageTitle";
import Divider from "../../components/shared/Divider/Divider";
import Paragraph from "../../components/Paragraph";
import { Box, Typography, CardMedia } from "@material-ui/core";

import image from "../../assets/Science/Science.svg";

const Science = (props) => {
  return (
    <React.Fragment>
      <PageTitle text="Научная деятельность" textAlign="center" />
      <Divider />
      <Box>
        <Paragraph text="На факультете ежегодно проводятся научно-практические конференции, круглые столы, конкурсы, организаторами которых выступают кафедры факультета:" />
        <ul>
          <li>
            <Typography component="p">
              ежегодная Международная научно-практическая конференция
              «Становление и развитие предпринимательства в России: история,
              современность и перспективы» (кафедра экономики);
            </Typography>
          </li>
          <li>
            <Typography component="p">
              Всероссийская научная конференция «Цифровое пространство:
              экономика, управление, социум»;
            </Typography>
          </li>
          <li>
            <Typography component="p">
              ежегодные «круглые столы», проводимые на базе Товарно -
              промышленной палата доц. А.С. Кузавко «Днепро-двинский регион –
              регион возможностей»;
            </Typography>
          </li>
          <li>
            <Typography component="p">
              ежегодно проводимые кафедрой экономики «Недели
              предпринимательства»;
            </Typography>
          </li>
          <li>
            <Typography component="p">
              ежегодные Дни финансовой грамотности с приглашением специалистов
              Центрального банка, руководителей Фонда поддержки
              предпринимателей, коммерческих банков;
            </Typography>
          </li>
          <li>
            <Typography component="p">
              конкурс «Цифровая экономика: современные платформы поддержки
              бизнеса» и др.
            </Typography>
          </li>
        </ul>
        <Paragraph text="Кафедры издают материалы научных конференций, сборники научных статей участников проводимых мероприятий, монографии." />
      </Box>
      <Box>
        <CardMedia
          component="img"
          alt="Круглый стол"
          image={image}
          title="Круглый стол"
        />
      </Box>
      <Divider />
    </React.Fragment>
  );
};

export default Science;
