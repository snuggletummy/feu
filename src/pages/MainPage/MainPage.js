import React from "react";
import Box from "@material-ui/core/Box";
import MainPageCarousel from "../../components/MainPageCarousel/MainPageCarousel";
import MainNews from "../../components/MainNews/MainNews";
import Graduates from "../../components/Graduates/Graduates";
import Contacts from "../../components/Contacts/Contacts";
import Divider from "../../components/shared/Divider/Divider";

const MainPage = () => {
  return (
    <React.Fragment>
      <MainPageCarousel />
      <Box mb={8}>
        <MainNews />
      </Box>
      <Divider />
      <Box mb={8}>
        <Graduates />
      </Box>
      <Divider />
      <Box mb={8}>
        <Contacts />
      </Box>
      <Divider />
    </React.Fragment>
  );
};

export default MainPage;
