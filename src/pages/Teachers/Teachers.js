import React from "react";
import TeachersExpandPanel from "../../components/TeachersExpandPanel";
import departmentOfEconomyMock from "../../mocks/departmentOfEconomyMock";
import departmentOfAnalyticalAndDigitalTechnologies from "../../mocks/departmentOfAnalyticalAndDigitalTechnologies";
import departmentOfManagementMock from "../../mocks/departmentOfManagementMock";
import facultyOfEconomicsAndManagementMock from "../../mocks/facultyOfEconomicsAndManagementMock";

const Teachers = (props) => {
  return (
    <React.Fragment>
      <TeachersExpandPanel
        allowExpand={false}
        isExpanded
        title="Факультет Экономики и Управления"
        teachersInfo={facultyOfEconomicsAndManagementMock}
      />
      <TeachersExpandPanel
        title="Кафедра экономики"
        teachersInfo={departmentOfEconomyMock}
      />
      <TeachersExpandPanel
        title="Кафедра менеджмента"
        teachersInfo={departmentOfManagementMock}
      />
      <TeachersExpandPanel
        title="Кафедра аналитических и цифровых технологий"
        teachersInfo={departmentOfAnalyticalAndDigitalTechnologies}
      />
    </React.Fragment>
  );
};

export default Teachers;
