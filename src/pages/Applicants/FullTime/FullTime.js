import React from "react";
import PageTitle from "../../../components/shared/Titles/PageTitle";
import Divider from "../../../components/shared/Divider/Divider";
import SpecialtyInfo from "../../../components/SpecialtyInfo/SpecialtyInfo";
import bachelorMock from "../../../mocks/Education/FullTime/bachelorMock";
import magistracyMock from "../../../mocks/Education/FullTime/magistracyMock";
import specialtyMock from "../../../mocks/Education/FullTime/specialtyMock";
import { Box } from "@material-ui/core";
import GeneralInformation from "../../../components/shared/GeneralInformation";

const FullTime = (props) => {
  return (
    <React.Fragment>
      <PageTitle text="Очное отделение" textAlign="center" />
      <Divider />
      <Box mb={3}>
        <SpecialtyInfo initialTab="bachelor" data={bachelorMock} />
      </Box>
      <Box mb={3}>
        <SpecialtyInfo initialTab="magistracy" data={magistracyMock} />
      </Box>
      <Box mb={3}>
        <SpecialtyInfo initialTab="specialty" data={specialtyMock} />
      </Box>
      <GeneralInformation title="Общая информация" />
    </React.Fragment>
  );
};

export default FullTime;
