import React from "react";
import Box from "@material-ui/core/Box";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import CardActions from "@material-ui/core/CardActions";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import fullTimeImage from "../../assets/Applicants/FullTime.jpg";
import extramuralImage from "../../assets/Applicants/Extramural.jpg";
import useStyles from "./Applicant.styles.js";
import { Link } from "react-router-dom";

const NeedToKnowFirstPoint = () => (
  <p>1. В СмолГУ прием ведется на очную заочную формы обучения.</p>
);

const NeedToKnowSecondPoint = () => (
  <>
    <p>
      2. Поступающие могут поступать на места в рамках контрольных цифр приема
      (бесплатное обучение) и на места по договорам об образовании (платное
      обучение).
    </p>
    <p>
      На места в рамках контрольных цифр приема (бесплатное обучение) могут
      поступать:
    </p>
    <ul>
      <li>граждане Российской Федерации;</li>
      <li>
        граждане иностранных государств в соответствии с международными
        договорами Российской Федерации;
      </li>
      <li>
        соотечественники, проживающие за рубежом, при соблюдении ими требований
        статьи 17 Федерального закона от 24 мая 1999 г. N 99-ФЗ «О
        государственной политике Российской Федерации в отношении
        соотечественников за рубежом»
      </li>
    </ul>
    <p>
      На места по договорам об образовании (платное обучение) могут поступать:
    </p>
    <ul>
      <li>граждане Российской Федерации;</li>
      <li>иностранные граждане и лица без гражданства.</li>
    </ul>
  </>
);

const EducationTypeCards = () => {
  const classes = useStyles();
  return (
    <Box className={classes.infoWrapper}>
      <Card raised className={classes.mediaCard}>
        <CardMedia image={fullTimeImage} className={classes.image} />
        <CardContent>
          <Typography variant="h4" component="p">
            Очное отделение
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" component={Link} to={"/applicants/full-time"}>
            Узнать подробнее
          </Button>
        </CardActions>
      </Card>
      <Card raised className={classes.mediaCard}>
        <CardMedia image={extramuralImage} className={classes.image} />
        <CardContent>
          <Typography variant="h4" component="p">
            Заочное отделение
          </Typography>
        </CardContent>
        <CardActions>
          <Button size="small" component={Link} to={"/applicants/extramural"}>
            Узнать подробнее
          </Button>
        </CardActions>
      </Card>
    </Box>
  );
};

const NeedToKnowThirdPoint = () => (
  <>
    <p>
      3. Прием в СмолГУ осуществляется по результатам вступительных испытаний. В
      качестве результатов вступительных испытаний признаются результаты Единого
      государственного экзамена (ЕГЭ). Отдельные категории абитуриентов могут
      поступать по результатам вступительных испытаний, проводимых СмолГУ
      самостоятельно:
    </p>
    <ul>
      <li>лица, имеющие профессиональное образование;</li>
      <li>дети-инвалиды, инвалиды;</li>
      <li>иностранные граждане;</li>
    </ul>
  </>
);

const NeedToKnowFourthPoint = () => (
  <>
    <p>4. Документы необходимые для поступления:</p>
    <ul>
      <li>заявление;</li>
      <li>
        документ о наличии среднего общего образования (аттестат, диплом о
        среднем профессиональном образовании, диплом о начальном
        профессиональном образовании (с указанием о получении лицом среднего
        общего образования));
      </li>
      <li>
        документ, удостоверяющий личность (паспорт или иной документ), и его
        копия;
      </li>
      <li>
        медицинская справка при поступлении на направления подготовки
        «Педагогическое образование (все профили)», «Психолого-педагогическое
        образование (все профили)», «Специальное (дефектологическое) образование
        (логопедия)».
      </li>
      <li>
        2 фотографии – для поступающих по результатам вступительных испытаний
        университета.
      </li>
    </ul>
  </>
);

const NeedToKnowFifthPoint = () => (
  <p>
    5. При поступлении учитываются индивидуальные достижения абитуриента в
    соответствии с правилами приема. Для того, чтобы выбрать направление
    подготовки, узнать перечень экзаменов, минимальные баллы ЕГЭ и другую
    необходимую для поступления информацию внимательно ознакомьтесь с
    документами, расположенными на этой странице сайта.
  </p>
);

const Applicants = (props) => {
  return (
    <Box>
      <Box>
        <p>Уважаемые поступающие!</p>
        <p>
          Для поступления в Смоленский государственный университет вам нужно
          знать следующее.
        </p>
        <NeedToKnowFirstPoint />
        <EducationTypeCards />
        <NeedToKnowSecondPoint />
        <NeedToKnowThirdPoint />
        <NeedToKnowFourthPoint />
        <NeedToKnowFifthPoint />
      </Box>
    </Box>
  );
};

export default Applicants;
