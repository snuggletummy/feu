import React from "react";
import PageTitle from "../../../components/shared/Titles/PageTitle";
import Divider from "../../../components/shared/Divider/Divider";
import SpecialtyInfo from "../../../components/SpecialtyInfo/SpecialtyInfo";
import bachelorMock from "../../../mocks/Education/Extramural/bachelorMock";
import magistracyMock from "../../../mocks/Education/Extramural/magistracyMock";
import { Box } from "@material-ui/core";
import GeneralInformation from "../../../components/shared/GeneralInformation";

const Extramural = (props) => {
  return (
    <React.Fragment>
      <PageTitle text="Заочное отделение" textAlign="center" />
      <Divider />
      <Box mb={3}>
        <SpecialtyInfo initialTab="bachelor" data={bachelorMock} />
      </Box>
      <Box mb={3}>
        <SpecialtyInfo initialTab="magistracy" data={magistracyMock} />
      </Box>
      <GeneralInformation title="Общая информация" />
    </React.Fragment>
  );
};

export default Extramural;
