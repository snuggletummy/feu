import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  infoWrapper: {
    display: "flex",
    flexWrap: "wrap",
    flexDirection: "row",
    justifyContent: "space-evenly"
  },
  mediaCard: {
    width: theme.spacing(44),
    height: "fit-content",
    margin: theme.spacing(1)
  },
  image: {
    height: theme.spacing(34),
    width: "100%"
  }
}));

export default useStyles;
