import React from "react";
import Box from "@material-ui/core/Box";
import StudentsSchedule from "../../components/StudentsSchedule";
import StudentsInfo from "../../components/StudentsInfo";
import studentsScholarshipMock from "../../mocks/Students/studentsScholarshipMock";
import studentsDormitoryMock from "../../mocks/Students/studentsDormitoryMock";
import studentsPayedToFreeMock from "../../mocks/Students/studentsPayedToFreeMock";
import studentsEducationCostMock from "../../mocks/Students/studentsEducationCostMock";
import stipend from "../../assets/ForStudents/stipend.jpg";
import hostel from "../../assets/ForStudents/hostel.jpg";
import payToFree from "../../assets/ForStudents/payToFree.jpg";
import price from "../../assets/ForStudents/price.jpg";

const Students = (props) => {
  return (
    <React.Fragment>
      <Box mb={5}>
        <StudentsSchedule />
      </Box>
      <Box mb={5}>
        <StudentsInfo
          title="Стипендия"
          image={stipend}
          textArray={studentsScholarshipMock.text}
          links={studentsScholarshipMock.links}
        />
      </Box>
      <Box mb={5}>
        <StudentsInfo
          image={hostel}
          title="Общежитие"
          links={studentsDormitoryMock.links}
        />
      </Box>
      <Box mb={5}>
        <StudentsInfo
          image={payToFree}
          title="Перевод с платного обучения на бесплатное"
          textArray={studentsPayedToFreeMock.text}
          links={studentsPayedToFreeMock.links}
        />
      </Box>
      <Box mb={5}>
        <StudentsInfo
          image={price}
          title="Стоимость обучения"
          links={studentsEducationCostMock.links}
        />
      </Box>
    </React.Fragment>
  );
};

export default Students;
