import React from "react";
import Box from "@material-ui/core/Box";
import AboutText from "../AboutText";

const AboutCommonInfo = ({ text, dark }) => {
  return (
    <Box
      display="flex"
      alignItems="center"
      minHeight="120px"
      bgcolor={"rgba(199, 218, 254)"}
    >
      <AboutText text={text} />
    </Box>
  );
};

export default AboutCommonInfo;
