import React from "react";
import Box from "@material-ui/core/Box";
import Paper from "@material-ui/core/Paper";
import useStyles from "./News.styles";
import { Typography } from "@material-ui/core";
import Paragraph from "../../Paragraph";

const News = ({ title, text, image }) => {
  const classes = useStyles();

  return (
    <Paper>
      <Box minHeight="660px">
        <Box height="400px" display="flex" mb={1}>
          <img
            style={{ width: "100%" }}
            alt="Новость"
            src={require(`../../../assets/${image}`)}
          />
        </Box>
        <Box pl={2} pr={2} height="15%">
          <Typography className={classes.title} component="p" variant="h5">
            {title}
          </Typography>
        </Box>
        <Box pl={2} pr={2}>
          <Paragraph text={text} />
        </Box>
      </Box>
    </Paper>
  );
};

export default News;
