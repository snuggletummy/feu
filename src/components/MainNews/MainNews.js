import React from "react";
import Box from "@material-ui/core/Box";
import Grid from "@material-ui/core/Grid";
import { NavLink } from "react-router-dom";
import News from "./News/News";
import MainPageTitle from "../shared/Titles/MainPageTitle";
import mainPageNewsMock from "../../mocks/MainPage/mainPageNewsMock";
import useStyles from "./MainNews.styles.js";

const MainNews = () => {
  const classes = useStyles();

  return (
    <React.Fragment>
      <MainPageTitle text="Новости" />
      <Grid container>
        <Grid
          className={classes.newsRow}
          xs={12}
          spacing={2}
          container
          item
          justify="center"
          alignItems="center"
        >
          {mainPageNewsMock.map((news) => {
            return (
              <Grid key={news.text} xs={12} md={6} lg={6} item>
                <News image={news.image} title={news.title} text={news.text} />
              </Grid>
            );
          })}
        </Grid>
      </Grid>
      <Box className={classes.more}>
        <NavLink to="/news">Все новости...</NavLink>
      </Box>
    </React.Fragment>
  );
};

export default MainNews;
