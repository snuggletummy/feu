import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    paddingLeft: theme.spacing(13),
    marginBottom: theme.spacing(8),
    fontSize: 78,
    color: "rgba(0, 0, 0, 0.8)"
  },
  more:{
    marginBottom: theme.spacing(8),
    fontSize: 22,
    textShadow: "0px 4px 4px rgba(0, 0, 0, 0.25)",
    textAlign: "center"
  },
  coloredBox: {
    backgroundColor: "red"
  },
  newsRow: {
    marginBottom: theme.spacing(2)
  } 
}));

export default useStyles;
