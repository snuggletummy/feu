import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import MuiExpansionPanel from "@material-ui/core/ExpansionPanel";
import MuiExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import MuiExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import useStyles from "./TeachersExpandPanel.styles";
import TeacherInfo from "../TeacherInfo/TeacherInfo";

const ExpansionPanel = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0
    },
    "&:before": {
      display: "none"
    },
    "&$expanded": {
      margin: "auto"
    }
  },
  expanded: {}
})(MuiExpansionPanel);

const ExpansionPanelSummary = withStyles({
  root: {
    backgroundColor: "rgba(151, 151, 151, 0.1)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56
    }
  },
  content: {
    "&$expanded": {
      margin: "12px 0"
    }
  },
  expanded: {}
})(MuiExpansionPanelSummary);

const ExpansionPanelDetails = withStyles(theme => ({
  root: {
    backgroundColor: "rgba(151, 151, 151, 0.1)",
    padding: theme.spacing(2)
  }
}))(MuiExpansionPanelDetails);


const TeachersExpandPanel = props => {
  const classes = useStyles();
  const { title, teachersInfo, isExpanded = false, allowExpand = true } = props;
  const [expanded, setExpanded] = useState(isExpanded);

  const handleChange = (event, state) => {
    setExpanded(state);
  }

  return (
    <React.Fragment>
      <ExpansionPanel expanded={expanded} onChange={allowExpand ? handleChange : null}>
        <ExpansionPanelSummary
          expandIcon={allowExpand ? <ExpandMoreIcon /> : null}
        >
          <Box className={classes.titleContainer}>
            <Typography className={classes.title}>{title}</Typography>
          </Box>
        </ExpansionPanelSummary>
        <ExpansionPanelDetails>
          <Box className={classes.teacherInfoContainer}>
            {teachersInfo.map((teacherInfo, index) => {
              return (
                teacherInfo.length - 1 === index ? (
                  <Box key={`${teacherInfo.name}-${teacherInfo.surname}-${teacherInfo.patronymic}`}>
                    <TeacherInfo {...teacherInfo} />
                  </Box>
                ) : (
                    <Box key={`${teacherInfo.name}-${teacherInfo.surname}-${teacherInfo.patronymic}`} mb={2}>
                      <TeacherInfo {...teacherInfo} />
                    </Box>
                  )
              );
            })}
          </Box>
        </ExpansionPanelDetails>
      </ExpansionPanel>
    </React.Fragment>
  );
}

export default TeachersExpandPanel;