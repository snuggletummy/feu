import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  teacherInfoContainer: {
    width: "100%"
  },
  titleContainer: {
    width: "100%",
    textAlign: "center"
  },
  title:{
    fontSize: 36
  }
}));

export default useStyles;
