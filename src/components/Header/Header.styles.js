import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  content: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-between",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
    },
  },
  contacts: {
    display: "flex",
    alignItems: "center",
    alignSelf: "flex-end",
    [theme.breakpoints.up("md")]: {
      alignSelf: "center",
    },
  },
  navLinks: {
    display: "flex",
    flexDirection: "column",
    textAlign: "center",
    [theme.breakpoints.up("md")]: {
      flexDirection: "row",
      justifyContent: "space-evenly",
    },
  },
  header: {
    paddingTop: theme.spacing(3),
    paddingBottom: theme.spacing(3),
    width: "100%",
  },
  navLink: {
    textDecoration: "none",
    color: theme.palette.common.black,
    borderBottom: "1px solid #979797",
    paddingBottom: theme.spacing(1),
    fontStyle: "normal",
    fontSize: "20px",
  },
  logo: {
    marginRight: theme.spacing(1),
    width: theme.spacing(10),
    height: theme.spacing(10),
  },
}));

export default useStyles;
