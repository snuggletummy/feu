import React from "react";
import Box from "@material-ui/core/Box";
import Logo from "../Logo/Logo";
import logoFEU from "../../assets/logo_feu.svg";
import Typography from "@material-ui/core/Typography";
import { NavLink } from "react-router-dom";
import useStyles from "./Header.styles";
import headerLinks from "../../consts/headerLinks";
import SocialMedias from "../SocialMedias";

const Header = () => {
  const classes = useStyles();
  return (
    <header className={classes.header}>
      <Box className={classes.content}>
        <Box display="flex">
          <Logo active logo={logoFEU} customClass={classes.logo} />
          <Box>
            <Typography variant="h4" component="p">
              Факультет Экономики и Управления
            </Typography>
            <Typography variant="h6" component="p">
              Смоленский государственный универитет
            </Typography>
          </Box>
        </Box>
        <Box className={classes.contacts}>
          <Typography variant="h6" component="p">
            (4812)700-270
          </Typography>
          <SocialMedias />
        </Box>
      </Box>
      <Box className={classes.navLinks}>
        {headerLinks.map((item) => {
          return (
            <NavLink
              className={classes.navLink}
              to={item.link}
              activeStyle={{
                fontWeight: "bold",
                color: "black",
              }}
            >
              {item.text}
            </NavLink>
          );
        })}
      </Box>
    </header>
  );
};

export default Header;
