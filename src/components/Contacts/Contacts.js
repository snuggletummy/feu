import React from "react";
import Grid from '@material-ui/core/Grid';
import MainPageTitle from "../shared/Titles/MainPageTitle";
import Map from "../Map/Map";
import ContactsInfo from "../shared/ContactsCards/ContactsInfo/ContactsInfo";
import CallIcon from '@material-ui/icons/Call';
import MailIcon from '@material-ui/icons/Mail';
import HomeIcon from '@material-ui/icons/Home';
// import SocialMedias from "../shared/ContactsCards/SocialMedias";
import SocialMedias from "../SocialMedias";

const Contacts = () => {
  return (
    <React.Fragment>
      <MainPageTitle text="Контакты" />
      <Grid container justify="center" >
        {/* 
          TODO: add react intl. 
          Smtng like 'titleMessageId' and 'messageId' prop 
        */}
        <Grid xs={12} spacing={2} container item>
          <Grid item xs>
            <ContactsInfo icon={<CallIcon />} title="Телефон:" text="(4812)700-270" />
          </Grid>
          <Grid item xs>
            <ContactsInfo icon={<MailIcon />} title="Почта:" text="dek-upr@smolgu.ru" />
          </Grid>
          <Grid item xs>
            <ContactsInfo icon={<HomeIcon />} title="Адрес:" text="ул. Пржевальского, 4." />
          </Grid>
          <Grid item xs>
            <SocialMedias showTitle/>
          </Grid>
        </Grid>
        <Grid item>
          <Map />
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default Contacts;