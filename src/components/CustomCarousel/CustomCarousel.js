import React from "react";
import Box from '@material-ui/core/Box';
import Carousel from 'react-material-ui-carousel'
//https://github.com/Learus/react-material-ui-carousel
const CustomCarousel = props => {
  const { wrapperStyle } = props
  return (
    <React.Fragment>
      <Box style={wrapperStyle}>
        <Carousel>
          {props.children}
        </Carousel>
      </Box>
    </React.Fragment>
  )
}

export default CustomCarousel;
