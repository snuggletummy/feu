import React from "react";
import { Box, Link, Typography } from "@material-ui/core";
import ArrowForwardIosIcon from "@material-ui/icons/ArrowForwardIos";
import useStyles from "./StudentsSchedule.styles";

const StudentsSchedule = () => {
  const classes = useStyles();
  return (
    <Box className={classes.container}>
      <Link href={"#"}>
        <Box className={classes.linkContainer}>
          <Box display="flex" alignItems="center">
            <Typography component="div" variant="h6">
              Расписание
            </Typography>
            <Box>
              <ArrowForwardIosIcon />
            </Box>
          </Box>
        </Box>
      </Link>
    </Box>
  );
};

export default StudentsSchedule;
