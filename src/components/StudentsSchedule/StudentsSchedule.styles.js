import { makeStyles } from "@material-ui/core/styles";
import scheduleImage from "../../assets/ForStudents/schedule.jpg";

const useStyles = makeStyles((theme) => ({
  container: {
    height: "400px",
    display: "flex",
    justifyContent: "flex-end",
    alignItems: "center",
    backgroundImage: `url(${scheduleImage})`,
    backgroundSize: "cover",
  },
  linkContainer: {
    display: "flex",
    borderRadius: "50px",
    backgroundColor: "white",
    marginRight: theme.spacing(10),
    width: "200px",
    height: "60px",
    justifyContent: "center",
    alignItems: "center",
  },
}));

export default useStyles;
