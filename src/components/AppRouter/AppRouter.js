import React from "react";
import routes from "../../routes";
import { Switch, withRouter } from "react-router-dom";
import RouteWithLayout from "../RouteWithLayout/RouteWithLayout";

const AppRouter = props => {
  const Routes = () =>
    routes.map(route =>
      route.exact ? (
        <RouteWithLayout
          component={route.component}
          name={route.name}
          exact
          layout={route.layout}
          path={route.link}
          key={route.key}
        />
      ) : (
        <RouteWithLayout
          component={route.component}
          name={route.name}
          layout={route.layout}
          path={route.link}
          key={route.key}
        />
      )
    );

  return (
    <Switch>
      <Routes />
    </Switch>
  );
};

export default withRouter(AppRouter);
