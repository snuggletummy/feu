import React from "react";
import Grid from "@material-ui/core/Grid";
import SignedPicture from "../SignedPicture";

import sciencePicture from "../../assets/AboutUs/Achievements/science.jpg";
import conferencesPicture from "../../assets/AboutUs/Achievements/conferences.jpg";
import sportPicture from "../../assets/AboutUs/Achievements/sport.jpg";
import staffPicture from "../../assets/AboutUs/Achievements/staff.jpg";
import AboutText from "../AboutText";

const AboutAchievements = (props) => {
  const achievements = [
    {
      image: sciencePicture,
      text: "На факультете работает научное студенческое общество, членами которого является более 60% студентов.",
    },
    {
      image: conferencesPicture,
      text: "Студенты факультета регулярно принимают участие в научных конференциях, конкурсах бизнес-проектов, олимпиадах не только внутри университета, но и в вузах г. Смоленска, Москвы и др.",
    },
    {
      image: sportPicture,
      text: "Многие студенты факультета являются участниками и победителями Всероссийских олимпиад, конкурсов бизнес-проектов.",
    },
    {
      image: staffPicture,
      text: "Профильную подготовку преподают 3 доктора наук, 21 кандидат наук",
    },
  ];
  return (
    <Grid container>
      {achievements.map((achievement) => {
        return (
          <Grid key={achievement.image} item xs={12} lg={6}>
            <SignedPicture
              image={achievement.image}
              textNode={<AboutText isWhite text={achievement.text} />}
            />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default AboutAchievements;
