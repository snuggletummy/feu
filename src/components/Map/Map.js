import React from "react";
import { YMaps, Map as YandexMap } from 'react-yandex-maps';

const Map = (props) => {
  return (
    <YMaps>
      <YandexMap width={"800px"} height={"400px"} defaultState={{ center: [54.784135, 32.045630], zoom: 16 }} />
    </YMaps>
  );
};

export default Map;