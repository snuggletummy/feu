import React from "react";
import { Box, Typography, Paper } from "@material-ui/core";
import ResourceLink from "../ResourceLink";
import Paragraph from "../Paragraph";

const StudentsInfo = ({ title, image = null, textArray = [], links = [] }) => {
  return (
    <Paper>
      <Box display="flex">
        <Box
          style={{ backgroundImage: `url(${image})`, backgroundSize: "cover" }}
          width="50%"
          minHeight="400px"
        ></Box>
        <Box pl={2} width="50%">
          <Typography variant="h4" component="p">
            {title}
          </Typography>
          <div style={{ marginBottom: "4px" }}>
            {textArray.map((text) => {
              return <Paragraph key={text} text={text} />;
            })}
          </div>
          {links.map((link) => {
            return (
              <ResourceLink
                key={link.href}
                link={link.href}
                title={link.text}
              />
            );
          })}
        </Box>
      </Box>
    </Paper>
  );
};

export default StudentsInfo;
