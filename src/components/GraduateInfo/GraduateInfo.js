import React from "react";
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardMedia';
import CardActionArea from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';

const GraduateInfo = ({
  name,
  surname,
  year,
  specialty,
  description,
  imagePath
}) => {
  return (
    <Card>
      <CardActionArea>
        <CardMedia
          component="img"
          alt={`${name} ${surname}`}
          image={require("../../assets/" + imagePath)}
        />
        <CardContent>
          <Typography gutterBottom variant="h5" component="h2">
            {`${name} ${surname}`}
          </Typography>
          <Typography variant="body2" color="textSecondary" component="p">
            {description}
          </Typography>
        </CardContent>
      </CardActionArea>
    </Card>
  )
}

export default GraduateInfo;