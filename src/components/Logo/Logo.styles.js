import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(() => ({
  logoContainer: {
    width: "100%",
    display: "inline-flex",
    fontStyle: "normal",
    fontWeight: "normal"
  }
}));

export default useStyles;
