import React from "react";
import Link from "@material-ui/core/Link";
import { Link as RouterLink } from "react-router-dom";
import clsx from 'clsx';

const Logo = ({ logo, active, customClass }) => {
  const logoClasses = clsx(customClass || "");
  return (
    active ? (
      <Link component={RouterLink} to="/">
        <img
          className={logoClasses}
          src={logo}
          alt="ФЭУ"
        />
      </Link>)
      : (
        <img
          className={logoClasses}
          src={logo}
          alt="ФЭУ"
        />
      )
  );
}

export default Logo;
