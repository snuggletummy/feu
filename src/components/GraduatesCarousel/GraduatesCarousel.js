import React from "react";
import SlickCarousel from "../SlickCarousel";
import GraduateInfo from "../GraduateInfo";
import graduatesInfoMock from "../../mocks/Graduates/graduatesInfoMock";

const GraduatesCarousel = () => {
  return (
    <SlickCarousel>
      {graduatesInfoMock.map(info => {
        const { name, surname, year, specialty, description, imagePath } = info;
        return (
          <GraduateInfo
            name={name}
            surname={surname}
            year={year}
            specialty={specialty}
            description={description}
            imagePath={imagePath}
          />
        )
      })}
    </SlickCarousel>
  );
}

export default GraduatesCarousel;