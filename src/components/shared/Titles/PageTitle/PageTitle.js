import React from "react";
import Box from '@material-ui/core/Box';
import useStyles from "./PageTitle.styles.js";

const PageTitle = props => {
  const classes = useStyles();
  return(
    <Box className={classes.title} textAlign={props.textAlign}>{props.text}</Box>
  );
}

export default PageTitle;