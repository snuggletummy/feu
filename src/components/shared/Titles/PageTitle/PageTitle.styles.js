import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: 60,
    color: "#828282"
  }
}));

export default useStyles;
