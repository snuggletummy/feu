import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  title: {
    paddingLeft: theme.spacing(8),
    marginBottom: theme.spacing(8),
    fontSize: 78,
    color: "rgba(0, 0, 0, 0.8)"
  }
}));

export default useStyles;
