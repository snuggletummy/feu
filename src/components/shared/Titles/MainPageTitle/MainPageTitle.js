import React from "react";
import Box from '@material-ui/core/Box';
import useStyles from "./MainPageTitle.styles.js";

const MainPageTitle = props => {
  const classes = useStyles();
  return(
    <Box className={classes.title}>{props.text}</Box>
  );
}

export default MainPageTitle;