import React from "react";
import Box from "@material-ui/core/Box";
import { Divider as DividerMUI } from "@material-ui/core";
import useStyles from "./Divider.styles.js";

const Divider = () => {
  const classes = useStyles();
  return (
    <Box mb={8}>
      <DividerMUI className={classes.divider} />
    </Box>
  );
};

export default Divider;
