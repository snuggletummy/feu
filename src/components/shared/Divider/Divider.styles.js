import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  divider: {
    marginLeft: "10%", 
    marginRight: "10%"
  }
}));

export default useStyles;
