import React from "react"
import { Box, Typography } from "@material-ui/core"
import useStyles from "./ContactsInfo.styles";

const ContactsInfo = props => {
  const classes = useStyles();
  return (
    <React.Fragment>
      <Box display="flex" justifyContent="center" alignItems="center">
        <Box mr={1}>
          {props.icon}
        </Box>
        <Box>
          <Typography className={classes.title} variant="h6">
            {props.title}
          </Typography>
          <Typography>
            {props.text}
          </Typography>
        </Box>
      </Box>
    </React.Fragment>
  )
}

export default ContactsInfo;