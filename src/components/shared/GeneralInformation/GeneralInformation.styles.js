import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  title: {
    marginBottom: theme.spacing(1),
    fontSize: "32px",
    textAlign: "center",
    color: "#979797"
  },
}));

export default useStyles;
