import React from "react";
import { Card, Box, Grid } from "@material-ui/core";
import useStyles from "./GeneralInformation.styles";
import linksMock from "../../../mocks/linksMock";
import ResourceLink from "../../ResourceLink";

const GeneralInformation = props => {
  const { title } = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <Card className={classes.title}>
        <Box>{title}</Box>
      </Card>
      <Card>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            {linksMock.firstColumn.map(data => {
              return (
                <ResourceLink 
                  key={`${data.key}-link`}
                  link={data.link}
                  title={data.title}
                />
              );
            })}
          </Grid>
          <Grid item xs={6}>
            {linksMock.secondColumn.map(data => {
              return (
                <ResourceLink 
                  key={`${data.key}-link`}
                  link={data.link}
                  title={data.title}
                />
              );
            })}
          </Grid>
        </Grid>
      </Card>
    </React.Fragment>
  );
}

export default GeneralInformation;