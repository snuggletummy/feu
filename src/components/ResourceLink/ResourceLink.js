import React from "react";
import { Box, Link } from "@material-ui/core";
import HelpIcon from '@material-ui/icons/Help';

const ResourceLink = ({ link, title, icon = null }) => {
  return (
    <Link href={link}>
      <Box display="flex">
        <Box flexGrow={1}>
          {title}
        </Box>
        <Box>
          {icon ? icon : <HelpIcon />}
        </Box>
      </Box>
    </Link>
  )
}

export default ResourceLink;