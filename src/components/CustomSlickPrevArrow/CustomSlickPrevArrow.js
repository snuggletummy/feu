import React from "react";
import clsx from "clsx";
import useStyles from "./CustomSlickPrevArrow.styles";

const CustomSlickPrevArrow = (props) => {
  const classes = useStyles();
  const { className, style, onClick } = props;
  return (
    <div
      className={clsx(className, classes.prevArrow)}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
};

export default CustomSlickPrevArrow;