import React from "react";
import Box from "@material-ui/core/Box";

const SignedPicture = ({ image, textNode }) => {
  return (
    <React.Fragment>
      <Box
        height="460px"
        maxWidth="100%"
        display="flex"
        alignItems="flex-end"
        style={{
          backgroundImage: `url(${image})`,
          backgroundRepeat: "no-repeat",
          backgroundSize: "cover",
        }}
      >
        <Box
          alignItems="center"
          justifyContent="center"
          minHeight="100px"
          bgcolor="rgba(0, 0, 0, 0.4)"
          width="100%"
          display="flex"
        >
          {textNode}
        </Box>
      </Box>
    </React.Fragment>
  );
};

export default SignedPicture;
