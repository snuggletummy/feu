import React from "react";
import { Card, CardMedia, CardContent, Box } from "@material-ui/core";
import useStyles from "./TeacherInfo.styles";
import Info from "./Info/Info";

const TeacherInfo = ({
  name,
  surname,
  patronymic,
  description,
  courses,
  interests,
  image,
}) => {
  const classes = useStyles();

  const mapInfo = (info) => {
    const infoString = Array.isArray(info) ? info.join(", ") : info;
    return `${infoString}.`;
  };

  const nameInfo = `${surname} ${name} ${patronymic}\u00A0-\u00A0`;

  return (
    <Card className={classes.infoCard}>
      <Box width="150px" p={1}>
        <CardMedia
          component="img"
          alt="Фото"
          className={classes.image}
          image={require("../../assets/" + image)}
          title={`${surname} ${name} ${patronymic}`}
        />
      </Box>
      <CardContent>
        <Info title={nameInfo} text={mapInfo(description)} />
        {Array.isArray(courses) && courses.length > 0 ? (
          <Info title={"Преподаваемые курсы:\u00A0"} text={mapInfo(courses)} />
        ) : null}
        {Array.isArray(interests) && interests.length > 0 ? (
          <Info
            title={"Профессиональные интересы:\u00A0"}
            text={mapInfo(interests)}
          />
        ) : null}
      </CardContent>
    </Card>
  );
};

export default TeacherInfo;
