import React from "react";
import {
  Box
} from "@material-ui/core";

import useStyles from "./Info.styles";

const Info = props => {
  const {title, text} = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <Box>
        <Box component="span" className={classes.title}>{title}</Box>
        {text}
      </Box>
    </React.Fragment>
  );
}

export default Info;