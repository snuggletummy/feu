import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  title: {
    fontWeight: "bold"
  }
}));

export default useStyles;
