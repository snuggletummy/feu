import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  infoCard: {
    display: 'flex',
    alignItems: "center"
  },
  image: {
    height: "150px",
    width: "150px"
  }
}));

export default useStyles;
