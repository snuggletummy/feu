import React from "react";
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import historyImage from "../../assets/AboutUs/Statistics/history.png";
import trainingAreasImage from "../../assets/AboutUs/Statistics/training-areas.png";
import studentsCountImage from "../../assets/AboutUs/Statistics/students-count.png";
import raitingImage from "../../assets/AboutUs/Statistics/raiting.png";
import SignedImage from "../SignedImage";

const AboutInfographics = props => {
  return (
    <Box p={5} bgcolor="rgba(199, 218, 254)">
      <Grid container spacing={2} justify="center">
        <Grid item xs={12} sm={6} md={3}>
          <SignedImage text="20 лет истории" image={historyImage} alternativeText="История" />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <SignedImage text="26 направлений" image={trainingAreasImage} alternativeText="Направления подготовки" />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <SignedImage text="800 студентов" image={studentsCountImage} alternativeText="Количество" />
        </Grid>
        <Grid item xs={12} sm={6} md={3}>
          <SignedImage text="Лидерство среди Смолеских Вузов" image={raitingImage} alternativeText="Рейтинг" />
        </Grid>
      </Grid>
    </Box>
  )
}

export default AboutInfographics;