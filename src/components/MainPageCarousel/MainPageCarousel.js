import React from "react";
import Box from "@material-ui/core/Box";
import CustomCarousel from "../CustomCarousel";
import MainPageCarouselContent from "../MainPageCarouselContent";
import mainPageCarouselContentMock from "../../mocks/MainPage/mainPageCarouselContentMock";

const MainPageCarousel = () => {
  return (
    <Box>
      <CustomCarousel>
        {mainPageCarouselContentMock.map((content) => (
          <MainPageCarouselContent key={content.text} text={content.text} />
        ))}
      </CustomCarousel>
    </Box>
  );
};

export default MainPageCarousel;
