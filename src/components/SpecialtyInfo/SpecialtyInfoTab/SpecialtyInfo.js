import React from 'react';
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";

const SpecialtyInfo = props => {
  const { children, value, index, ...other } = props;
  return (
    <React.Fragment>
      <Typography
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`full-width-tabpanel-${index}`}
        aria-labelledby={`full-width-tab-${index}`}
        {...other}
      >
        <Box pt={3} pb={3}>
          {children}
        </Box>
      </Typography>
    </React.Fragment>
  );
}

export default SpecialtyInfo;