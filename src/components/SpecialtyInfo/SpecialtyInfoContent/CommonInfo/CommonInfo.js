import React from "react";
import { Box, Grid } from "@material-ui/core";
import useStyles from "./CommonInfo.styles";

const CommonInfo = props => {
  const { title, text, isFirst } = props;
  const classes = useStyles();
  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs={6} className={classes.item}>
          <Box mt={isFirst ? 4 : 0} className={classes.content}>{title}</Box>
        </Grid>
        <Grid item xs={6} className={classes.item}>
          <Box mt={isFirst ? 4 : 0} className={classes.content}>{text}</Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default CommonInfo;