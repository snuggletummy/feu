import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles(theme => ({
  content: {
    paddingBottom: theme.spacing(2),
    paddingLeft: theme.spacing(2),
  },
  item: {
    borderBottom: "1px solid #707070",
    marginBottom: theme.spacing(4),
  }
}));

export default useStyles;
