import React from "react";
import { Box, Card, Grid } from "@material-ui/core";
import CommonInfo from "./CommonInfo";
import AdditionalInfo from "./AdditionalInfo";
import CheckIcon from '@material-ui/icons/Check';

const ExamsInfo = props => {
  const { items } = props;
  return (
    items.map(el => {
      return <Box key={`${el.name}-${el.count}`}>{el.name} <Box component="span">{`(минимальный балл: ${el.count})`}</Box></Box>
    })
  )
};

const PlacesCountInfo = props => {
  const { items } = props;
  return (
    items.map(el => {
      return <Box key={`${el.name}-${el.count}`}>{`${el.count} ${el.name}`}</Box>
    })
  )
};

const SpecialtyInfoContent = props => {
  const { info } = props;
  return (
    <React.Fragment>
      <Card>
        <Grid container spacing={4}>
          <Grid item xs={6}>
            <CommonInfo isFirst title="Направление подготовки" text={`${info.number} ${info.name}`} />
            <CommonInfo title="Форма обучения" text={info.form} />
            <CommonInfo title="Продолжительность обучения" text={info.lengthOfStudy} />
            <CommonInfo title="Вступительные испытания" text={<ExamsInfo items={info.exams} />} />
            <CommonInfo title="Количество мест" text={<PlacesCountInfo items={info.count} />} />
          </Grid>
          <Grid container direction="column" item xs={6}>
            <Grid item>
              <AdditionalInfo icon={<CheckIcon />} title="Выпускник умеет:" info={info.graduateSkills} />
            </Grid>
            <Grid item>
              <AdditionalInfo icon={<CheckIcon />} title="Трудоустройство выпускников:" info={info.employmentOfGraduates} />
            </Grid>
          </Grid>
        </Grid>
      </Card>
    </React.Fragment>
  );
}

export default SpecialtyInfoContent;