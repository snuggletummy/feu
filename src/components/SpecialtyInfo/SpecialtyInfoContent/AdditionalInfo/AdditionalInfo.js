import React from "react";
import { Box } from "@material-ui/core";

const AdditionalInfo = props => {
  const { title, info, icon } = props;
  return (
    <React.Fragment>
      <Box mb={3}>
        <Box fontSize={20} mb={3}>{title}</Box>
        {info.map(element => {
          return (
            <Box key={element} display="flex">
              {icon}
              <Box>{element}</Box>
            </Box>
          );
        })}
      </Box>
    </React.Fragment>
  );
}

export default AdditionalInfo;