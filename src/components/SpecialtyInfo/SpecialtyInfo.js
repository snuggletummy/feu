import React from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import SpecialtyInfoTab from "./SpecialtyInfoTab";
import SpecialtyInfoContant from "./SpecialtyInfoContent";

const SpecialtyInfo = props => {
  const { data, initialTab } = props;
  const [tabDetails, setTabDetails] = React.useState(initialTab);
  return (
    <React.Fragment>
      <Tabs
        value={tabDetails}
        onChange={(event, value) => setTabDetails(value)}
        variant="scrollable"
        scrollButtons="auto"
      >
        {data.map(tab => {
          return (
            <Tab
              key={tab.enTitle}
              value={tab.enTitle}
              label={tab.title}
            />
          );
        })}
      </Tabs>
      {data.map(tab => {
        return (
          tab.info ? (
            <SpecialtyInfoTab
              value={tabDetails}
              index={tab.enTitle}
              key={`tabcontent-${tab.enTitle}`}
            >
              <SpecialtyInfoContant info={tab.info} />
            </SpecialtyInfoTab>
          ) : null
        )
      })}
    </React.Fragment>
  );
}

export default SpecialtyInfo;