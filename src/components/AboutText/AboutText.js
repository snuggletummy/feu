import React from "react";
import useStyles from "./AboutText.styles.js";
import Typography from "@material-ui/core/Typography";

const AboutText = ({ text, isWhite }) => {
  const classes = useStyles();

  return (
    <Typography className={isWhite ? classes.white : ""} align="center">
      {text}
    </Typography>
  );
};

export default AboutText;
