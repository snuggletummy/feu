import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  white: {
    textShadow: "1px 1px 0 black, 1px -1px 0 black, -1px 1px 0 black, -1px -1px 0 black",
    color: "white",
    transition: "all 1s"
  }
}));

export default useStyles;
