import React from "react";
import clsx from "clsx";
import useStyles from "./CustomSlickNextArrow.styles";

const CustomSlickNextArrow = (props) => {
  const classes = useStyles();
  const { className, style, onClick } = props;
  return (
    <div
      className={clsx(className, classes.nextArrow)}
      style={{ ...style, display: "block" }}
      onClick={onClick}
    />
  );
}

export default CustomSlickNextArrow;