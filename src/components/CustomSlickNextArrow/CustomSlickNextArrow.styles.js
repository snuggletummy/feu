import { makeStyles } from "@material-ui/core/styles";

const useStyles = makeStyles((theme) => ({
  nextArrow: {
    right: 0,
    zIndex: theme.zIndex.tooltip,
    "&:before":{
      color: theme.palette.secondary.main
    }
  }
}));

export default useStyles;
