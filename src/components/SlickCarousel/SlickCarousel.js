import React from "react";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import CustomSlickNextArrow from "../CustomSlickNextArrow";
import CustomSlickPrevArrow from "../CustomSlickPrevArrow";

const SlickCarousel = ({ children }) => {
  const settings = {
    dots: true,
    arrows: true,
    autoplay: true,
    className: "center",
    centerPadding: "60px",
    infinite: true,
    speed: 500,
    centerMode: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    nextArrow: <CustomSlickNextArrow />,
    prevArrow: <CustomSlickPrevArrow />
  };
  return (
    <Slider {...settings}>
      {children}
    </Slider>
  );
}

export default SlickCarousel;