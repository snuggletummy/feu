import React from "react";
import Typography from "@material-ui/core/Typography";

const Paragraph = ({text}) => {
  return <Typography style={{textIndent: "30px"}}>{text}</Typography>
}

export default Paragraph;