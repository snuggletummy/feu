import React from "react"
import Grid from "@material-ui/core/Grid";
import Logo from "../Logo/Logo";
import logoFEU from "../../assets/logo_feu.svg";
import logoSMOLGU from "../../assets/logo_smolgu.svg";
import { Box } from "@material-ui/core";
import { NavLink } from 'react-router-dom';

const Link = (props) => {
  const { title, link } = props;
  return (
    <Box mb={1} textAlign="center">
      <NavLink to={link}>{title}</NavLink>
    </Box>
  );

}

const Footer = props => {
  return (
    <React.Fragment>
      <Grid container>
        <Grid item xs>
          <Box textAlign="center">
            <Logo logo={logoFEU} />
          </Box>
        </Grid>
        <Grid item xs>
          <Link link="/about" title="О нас" />
          <Link link="/news" title="Новости" />
          <Link link="/applicants/full-time" title="Очное отделение" />
          <Link link="/applicants/extramural" title="Заочное отделение" />
        </Grid>
        <Grid item xs>
          <Box textAlign="center">
            {/* <Link link="/contacts" title="Контакты" /> */}
            <Link link="/science" title="Наука" />
            <Link link="/teachers" title="Руководство факультета" />
            <Link link="/students" title="Студентам" />
          </Box>
        </Grid>
        <Grid item xs>
          <Box textAlign="center">
            <Logo logo={logoSMOLGU} />
          </Box>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}

export default Footer;