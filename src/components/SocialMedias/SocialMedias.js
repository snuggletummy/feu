import React from "react";
import Link from "@material-ui/core/Link";
import VkIcon from "../VkIcon";
import InstagramIcon from "@material-ui/icons/Instagram";
import TelegramIcon from "@material-ui/icons/Telegram";
import { vkLink, instagramLink, telegramLink } from "../../utils/constants";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import useStyles from "./SocialMedias.styles";

const SocialMedias = ({ showTitle }) => {
  const classes = useStyles();

  const renderLinks = () => (
    <React.Fragment>
      <Link href={vkLink}>
        <VkIcon fontSize="large" />
      </Link>
      <Link href={instagramLink}>
        <InstagramIcon fontSize="large" />
      </Link>
      <Link href={telegramLink}>
        <TelegramIcon fontSize="large" />
      </Link>
    </React.Fragment>
  );

  const renderWithTitle = () => (
    <Box
      height="100%"
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      {/* TODO: add react intl */}
      <Typography variant="h6" component="p" className={classes.title}>
        {" "}
        Наши соцсети:&nbsp;
      </Typography>
      {renderLinks()}
    </Box>
  );

  return showTitle ? renderWithTitle() : renderLinks();
};

export default SocialMedias;
