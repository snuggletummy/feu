import React from "react";
import scheduleImage from "../../assets/MainPage/main.jpg";
import SignedPicture from "../SignedPicture";

const MainPageCarouselContent = ({ text }) => {
  return (
    <SignedPicture
      image={scheduleImage}
      textNode={
        <p
          style={{
            fontSize: "28px",
            color: "white",
            marginLeft: "16px",
          }}
        >
          {text}
        </p>
      }
    />
  );
};

export default MainPageCarouselContent;
