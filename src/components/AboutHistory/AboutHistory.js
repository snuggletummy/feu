import React from "react";
import Box from "@material-ui/core/Box";
import titlePicture from "../../assets/AboutUs/title.jpg";
import AboutText from "../AboutText";
import SignedPicture from "../SignedPicture";

const AboutHistory = () => {
  const dateOfEstablishment =
    "Факультет управления был создан в 2000 году с целью подготовки управленческих кадров.";
  const renameYear =
    "1 февраля 2012 г. факультет управления был переименован в факультет экономики и управления.";

  return (
    <SignedPicture
      image={titlePicture}
      textNode={
        <Box>
          <AboutText isWhite text={dateOfEstablishment} />
          <AboutText isWhite text={renameYear} />
        </Box>
      }
    />
  );
};

export default AboutHistory;
