import React from "react";
import Box from '@material-ui/core/Box';
import MainPageTitle from "../shared/Titles/MainPageTitle";
import GraduatesCarousel from "../GraduatesCarousel";

const Graduates = props => {
  return (
    <React.Fragment>
      <MainPageTitle text="Наши выпускники" />
      <Box>
        <GraduatesCarousel />
      </Box>
    </React.Fragment >
  )
};

export default Graduates; 