import React from "react";
import Box from '@material-ui/core/Box';
import PropTypes from 'prop-types';
import AboutText from "../AboutText";

const SignedImage = ({ text, image, alternativeText }) => {
  return (
    <React.Fragment>
      <Box display="flex" justifyContent="center" alignItems="center">
        <img src={image} alt={alternativeText}></img>
      </Box>
      <AboutText text={text}/>
    </React.Fragment>
  )
}

SignedImage.propTypes = {
  text: PropTypes.string.isRequired,
  image: PropTypes.string.isRequired,
  alternativeText: PropTypes.string.isRequired
};

export default SignedImage;