const headerLinks = [
    {
        text: "О нас",
        link: "/about"
    },
    {
        text: "Новости",
        link: "/news"
    },
    {
        text: "Абитуриентам",
        link: "/applicants"
    },
    {
        text: "Студентам",
        link: "/students"
    },
    {
        text: "Наука",
        link: "/science"
    },
    {
        text: "Преподаватели",
        link: "/teachers"
    },
    // {
    //     text: "Контакты",
    //     link: "/contacts"
    // }
];

export default headerLinks;
