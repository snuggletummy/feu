import MainLayout from "./pages/MainLayout/";
import MainPage from "./pages/MainPage";
import About from "./pages/About";
import News from "./pages/News";
import Applicants from "./pages/Applicants";
import Students from "./pages/Students";
import Science from "./pages/Science";
import Сontacts from "./pages/Сontacts";
import Teachers from "./pages/Teachers";
import FullTime from "./pages/Applicants/FullTime";
import Extramural from "./pages/Applicants/Extramural";

const routes = [
  {
    key: "mainPage",
    link: "/",
    component: MainPage,
    name: "Home",
    exact: true,
    layout: MainLayout
  },
  {
    key: "about",
    link: "/about",
    component: About,
    name: "About",
    exact: true,
    layout: MainLayout
  },
  {
    key: "news",
    link: "/news",
    component: News,
    name: "News",
    exact: true,
    layout: MainLayout
  },
  {
    key: "applicants",
    link: "/applicants",
    component: Applicants,
    name: "Applicants",
    exact: true,
    layout: MainLayout
  },
  {
    key: "applicants/full-time",
    link: "/applicants/full-time",
    component: FullTime,
    name: "FullTime",
    exact: true,
    layout: MainLayout
  },
  {
    key: "applicants/extramural",
    link: "/applicants/extramural",
    component: Extramural,
    name: "Extramural",
    exact: true,
    layout: MainLayout
  },
  {
    key: "students",
    link: "/students",
    component: Students,
    name: "Students",
    exact: true,
    layout: MainLayout
  },
  {
    key: "science",
    link: "/science",
    component: Science,
    name: "Science",
    exact: true,
    layout: MainLayout
  },
  {
    key: "teachers",
    link: "/teachers",
    component: Teachers,
    name: "Teachers",
    exact: true,
    layout: MainLayout
  },
  {
    key: "contacts",
    link: "/contacts",
    component: Сontacts,
    name: "Contacts",
    exact: true,
    layout: MainLayout
  },
];

export default routes;
